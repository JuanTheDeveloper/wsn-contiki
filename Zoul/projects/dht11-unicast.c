
#include "contiki.h"
#include "net/rime/rime.h"
#include <stdio.h>
#include "dev/dht11.h"
#include "sys/clock.h"

#define LED_PORT_BASE          GPIO_PORT_TO_BASE(GPIO_D_NUM)
#define RED_PIN_MASK           GPIO_PIN_MASK(5)
#define GREEN_PIN_MASK         GPIO_PIN_MASK(4)

uint8_t query; 
uint16_t temperature, humidity;
/*---------------------------------------------------------------------------*/
PROCESS(dht11_unicast_process, "Example unicast");
AUTOSTART_PROCESSES(&dht11_unicast_process);
/*---------------------------------------------------------------------------*/
static void
recv_uc(struct unicast_conn *c, const linkaddr_t *from)
{
  if(((from->u8[0])-(from->u8[1])) == 16){
  	query = 1;	
  }
}
/*---------------------------------------------------------------------------*/
static void
sent_uc(struct unicast_conn *c, int status, int num_tx)
{
  const linkaddr_t *dest = packetbuf_addr(PACKETBUF_ADDR_RECEIVER);
  if(linkaddr_cmp(dest, &linkaddr_null)){
    return;
  }
}
/*---------------------------------------------------------------------------*/
static const struct unicast_callbacks unicast_callbacks = {recv_uc, sent_uc};
static struct unicast_conn uc;
/*---------------------------------------------------------------------------*/
void delay_msec(uint16_t t){
	uint16_t x;
	
	for(x=0;x<(t*10);x++){
		clock_delay_usec(100);
	}
}
/*---------------------------------------------------------------------------*/
void led_toggle(uint8_t color, uint8_t t, uint8_t x){

	uint8_t i;
	
	for(i=0; i<x; i++){
  		GPIO_SET_PIN(LED_PORT_BASE, color);
  		delay_msec(t);
  		GPIO_CLR_PIN(LED_PORT_BASE, color);
  		delay_msec(t);
  	}
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(dht11_unicast_process, ev, data)
{
  PROCESS_EXITHANDLER(unicast_close(&uc);)
    
  PROCESS_BEGIN();
  SENSORS_ACTIVATE(dht11);
  
  char buffer[24]; 
  int j;
  
  query = 0;

  unicast_open(&uc, 146, &unicast_callbacks);

  while(1) {
    static struct etimer et;
    linkaddr_t addr;
    
    etimer_set(&et, CLOCK_SECOND);
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
    temperature = 0;
    humidity = 0;
    
    if((dht11_read_all(&temperature, &humidity) != DHT11_ERROR && query == 1)){ // && query == 1
    	j = snprintf(buffer, 24, "%u.%u, degC, %u.%u, RH", temperature/100, temperature%100, humidity/100, humidity%100);
    	led_toggle(GREEN_PIN_MASK, 10, 1);
    	packetbuf_copyfrom(&buffer,j);
    	addr.u8[0] = 0xe4;					// Client rime configured address e4:d4
    	addr.u8[1] = 0xd4;
    	if(!linkaddr_cmp(&addr, &linkaddr_node_addr)) {
      		unicast_send(&uc, &addr);
    	}
    	query = 0;
    }
  }

  PROCESS_END();
}
