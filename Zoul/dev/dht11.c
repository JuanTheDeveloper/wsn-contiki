/*
 * Copyright (c) 2016, Zolertia - http://www.zolertia.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
/*---------------------------------------------------------------------------*/
/**
 * \addtogroup zoul-dht11
 * @{
 *
 * \file
 *  Driver for the DHT11 temperature and humidity sensor
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "dht11.h"
#include "dev/gpio.h"
#include "lib/sensors.h"
#include "dev/ioc.h"
#include "dev/watchdog.h"
#include "sys/clock.h"
#include <stdio.h>
/*---------------------------------------------------------------------------*/
#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif
/*---------------------------------------------------------------------------*/
#define BUSYWAIT_UNTIL(max_time) \
  do { \
    rtimer_clock_t t0; \
    t0 = RTIMER_NOW(); \
    while(RTIMER_CLOCK_LT(RTIMER_NOW(), t0 + (max_time))) { \
      watchdog_periodic(); \
    } \
  } while(0)
/*---------------------------------------------------------------------------*/
#define DHT11_PORT_BASE          GPIO_PORT_TO_BASE(DHT11_PORT)
#define DHT11_PIN_MASK           GPIO_PIN_MASK(DHT11_PIN)
/*---------------------------------------------------------------------------*/
static uint8_t enabled;
static uint8_t busy;
static uint8_t raw_data[82];
static uint8_t dht11_data[5];
/*---------------------------------------------------------------------------*/
void delay_ms(uint16_t t){
	uint16_t x;
	
	for(x=0;x<(t*10);x++){
		clock_delay_usec(100);
	}
} 

static int
dht11_read(void)
{
  uint8_t i;
  uint8_t j = 0;
  uint8_t last_state;
  uint8_t counter = 0;
  uint8_t checksum = 0;

  if(enabled) {
  
  	for(i=0; i<5; i++) dht11_data[i] = 0;
  	for(i=0; i<82; i++)  raw_data[i] = 0;
    /* Exit low power mode and initialize variables */
    GPIO_SET_OUTPUT(DHT11_PORT_BASE, DHT11_PIN_MASK);
    GPIO_SET_PIN(DHT11_PORT_BASE, DHT11_PIN_MASK);
    delay_ms(DHT11_AWAKE_TIME);
    //BUSYWAIT_UNTIL(DHT11_AWAKE_TIME);
    //memset(dht11_data, 0, DHT11_BUFFER); 

    /* Initialization sequence */
    GPIO_CLR_PIN(DHT11_PORT_BASE, DHT11_PIN_MASK);
    delay_ms(DHT11_START_TIME);
    //BUSYWAIT_UNTIL(DHT11_START_TIME);
    
    GPIO_SET_PIN(DHT11_PORT_BASE, DHT11_PIN_MASK);
    clock_delay_usec(DHT11_READY_TIME);

    /* Prepare to read, DHT11 should keep line low 80us, then 80us high.
     * The ready-to-send-bit condition is the line kept low for 50us, then if
     * the line is high between 26-28us the bit sent will be "0" (zero), else
     * if the line is high between 70us the bit sent will be "1" (one).
     */
     
     
    GPIO_SET_INPUT(DHT11_PORT_BASE, DHT11_PIN_MASK);
    last_state = GPIO_READ_PIN(DHT11_PORT_BASE, DHT11_PIN_MASK);  // last_state = 0;

    for(i = 0; i < 82; i++) {
      counter = 0;
      while(GPIO_READ_PIN(DHT11_PORT_BASE, DHT11_PIN_MASK) == last_state) {
        counter++;
        clock_delay_usec(1);

        /* Exit if not responsive */
        if(counter == 0xFF) {
          break;
        }
      }
      last_state = GPIO_READ_PIN(DHT11_PORT_BASE, DHT11_PIN_MASK);

      /* Double check for stray sensor */
      if(counter == 0xFF) {
        break;
      }
      raw_data[i] = counter; 
    }
    
    /*Ignore first 2 transition and store odd arrays that contains the bits*/
    /*for this code timing counter > 8 means bit '1'*/
    for(i=0; i<82; i++){
    	if((i >= 2) && ((i % 2) != 0)) {
        	dht11_data[j / 8] <<= 1;
        	if(raw_data[i] > 8) {
        		dht11_data[j / 8] |= 1;
        	}
        	j++;
        }
    }
     
    checksum = dht11_data[0] + dht11_data[1] + dht11_data[2] + dht11_data[3];
    //checksum &= 0xFF;
    if(dht11_data[4] == checksum) {
        GPIO_SET_INPUT(DHT11_PORT_BASE, DHT11_PIN_MASK);
        GPIO_SET_PIN(DHT11_PORT_BASE, DHT11_PIN_MASK);
        return DHT11_SUCCESS;
    }
    PRINTF("DHT11: bad checksum\n");
  }
  return DHT11_ERROR;
}
/*---------------------------------------------------------------------------*/
static uint16_t
dht11_humidity(void)
{
  uint16_t res;

  res = dht11_data[0]*100 + dht11_data[1];

  busy = 0;
  return res;
}
/*---------------------------------------------------------------------------*/
static uint16_t
dht11_temperature(void)
{
  uint16_t res;
  
  res = dht11_data[2]*100 + dht11_data[3];
  
  busy = 0;
  return res;
}
/*---------------------------------------------------------------------------*/
static int
value(int type)
{
  if((type != DHT11_READ_HUM) && (type != DHT11_READ_TEMP) &&
     (type != DHT11_READ_ALL)) {
    PRINTF("DHT11: Invalid type %u\n", type);
    return DHT11_ERROR;
  }

  if(busy) {
    PRINTF("DHT11: ongoing operation, wait\n");
    return DHT11_BUSY;
  }

  busy = 1;

  if(dht11_read() != DHT11_SUCCESS) {
    PRINTF("DHT11: Fail to read sensor\n");
    GPIO_SET_INPUT(DHT11_PORT_BASE, DHT11_PIN_MASK);
    GPIO_SET_PIN(DHT11_PORT_BASE, DHT11_PIN_MASK);
    busy = 0;
    return DHT11_ERROR;
  }

  switch(type) {
  case DHT11_READ_HUM:
    return dht11_humidity();
  case DHT11_READ_TEMP:
    return dht11_temperature();
  case DHT11_READ_ALL:
    return DHT11_SUCCESS;
  default:
    return DHT11_ERROR;
  }
}
/*---------------------------------------------------------------------------*/
int
dht11_read_all(uint16_t *temperature, uint16_t *humidity)
{
  if((temperature == NULL) || (humidity == NULL)) {
    PRINTF("DHT11: Invalid arguments\n");
    return DHT11_ERROR;
  }

  if(value(DHT11_READ_ALL) != DHT11_ERROR) {
    *temperature = dht11_temperature();
    *humidity = dht11_humidity();
    return DHT11_SUCCESS;
  }

  /* Already cleaned-up in the value() function */
  return DHT11_ERROR;
}
/*---------------------------------------------------------------------------*/
static int
configure(int type, int value)
{
  if(type != SENSORS_ACTIVE) {
    return DHT11_ERROR;
  }

  GPIO_SOFTWARE_CONTROL(DHT11_PORT_BASE, DHT11_PIN_MASK);
  GPIO_SET_INPUT(DHT11_PORT_BASE, DHT11_PIN_MASK);
  ioc_set_over(DHT11_PORT, DHT11_PIN, IOC_OVERRIDE_OE);
  GPIO_SET_PIN(DHT11_PORT_BASE, DHT11_PIN_MASK);

  /* Restart flag */
  busy = 0;

  if(value) {
    enabled = 1;
    return DHT11_SUCCESS;
  }

  enabled = 0;
  return DHT11_SUCCESS;
}
/*---------------------------------------------------------------------------*/
SENSORS_SENSOR(dht11, DHT11_SENSOR, value, configure, NULL);
/*---------------------------------------------------------------------------*/
/** @} */
